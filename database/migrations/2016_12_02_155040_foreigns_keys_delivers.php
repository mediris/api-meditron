<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ForeignsKeysDelivers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('delivers', function (Blueprint $table) {

            $table->foreign('requested_procedure_id')->references('id')->on('requested_procedures')->onDelete('restrict');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('delivers', function (Blueprint $table) {

            $table->dropForeign('delivers_requested_procedure_id_foreign');

        });
    }
}
