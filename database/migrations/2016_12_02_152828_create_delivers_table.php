<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDeliversTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('delivers', function (Blueprint $table) {

            $table->increments('id');
            $table->dateTime('date');
            $table->string('receptor_id', 45)->index();
            $table->string('receptor_name', 10)->index();
            $table->boolean('file');
            $table->boolean('cd');
            $table->integer('num_cd');
            $table->boolean('plates');
            $table->integer('num_plates');
            $table->integer('responsable_id')->index()->unsigned();
            $table->string('responsable_name', 45)->index();
            $table->integer('requested_procedure_id')->index()->unsigned();
            $table->text('observations');
            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('delivers');
    }
}
