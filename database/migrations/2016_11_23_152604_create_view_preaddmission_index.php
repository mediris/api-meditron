<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateViewPreaddmissionIndex extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement('
        
            CREATE VIEW preadmissionIndexView AS
              SELECT 
                    `patient_types`.`icon` AS `patientTypeIcon`,
                    `patient_types`.`description` AS `patientType`,
                    `patient_types`.`administrative_ID` AS `patientTypeAID`,
                    `requested_procedures`.`id` AS `orderID`,
                    `requested_procedures`.`service_request_id` AS `serviceRequestID`,
                    concat(`service_requests`.`patient_first_name`,\' \',`service_requests`.`patient_last_name`)  AS `patientName`,
                    `service_requests`.`patient_identification_id` AS `patientIdentificationID`,
                    `procedures`.`id` AS `procedureID`,
                    `procedures`.`administrative_ID` AS `procedureAID`,
                    `procedures`.`description` AS `procedureDescription`,
                    `service_requests`.`patient_id` AS `patientID`,
                    `requested_procedure_statuses`.`description` AS `orderStatus`,
                    `requested_procedure_statuses`.`id` AS `orderStatusID`,
                    `referrings`.`administrative_ID` AS `referringID`,
                    concat(`referrings`.`first_name`,\' \',`referrings`.`last_name`)  AS `referringName`
              FROM
                   (((((`requested_procedures`
                   JOIN `service_requests` ON ((`requested_procedures`.`service_request_id` = `service_requests`.`id`)))
                   JOIN `procedures` ON ((`requested_procedures`.`procedure_id` = `procedures`.`id`)))
                   JOIN `requested_procedure_statuses` ON ((`requested_procedure_statuses`.`id` = `requested_procedures`.`requested_procedure_status_id`)))
                   JOIN `patient_types` ON ((`service_requests`.`patient_type_id` = `patient_types`.`id`)))
                   left JOIN `referrings` ON (( `service_requests`.`referring_id` = `referrings`.`id` )))
              WHERE
                   ((`requested_procedure_statuses`.`id` = 1) AND (`patient_types`.`admin_aprob` = 1))
                        
              ORDER BY 
                  serviceRequestID ASC    
        ');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement('DROP VIEW preadmissionIndexView');
    }
}
