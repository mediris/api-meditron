<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAppointmentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('appointments', function (Blueprint $table) {
            $table->increments('id');
            $table->boolean('active')->default(true);
            $table->dateTime('appointment_date_start')->index();
            $table->dateTime('appointment_date_end')->index();
            $table->integer('appointment_status_id')->unsigned()->index();
            $table->string('patient_identification_id', 45)->index();
            $table->string('patient_first_name', 25)->index();
            $table->string('patient_last_name', 25)->index();
            $table->string('patient_telephone_number', 45)->index();
            $table->string('patient_cellphone_number', 45)->index();
            $table->string('patient_email', 45)->index();
            $table->text('observations');
            $table->string('applicant', 45);
            $table->integer('patient_id')->index()->unsigned();
            $table->integer('user_id')->index()->unsigned();
            $table->integer('room_id')->index()->unsigned();
            $table->integer('procedure_id')->index()->unsigned();
            $table->dateTime('cancelation_date')->index();
            $table->string('cancelation_reason', 500);
            $table->integer('cancelation_user_id')->index()->unsigned();
            $table->integer('procedure_duration');
            $table->string('blocks_numbers', 250);
            $table->integer('equipment_id');
            $table->string('string_start_unique', 250)->unique();
            $table->string('string_end_unique', 250)->unique();
            $table->boolean('procedure_contrast_study');
            $table->text('patient_allergies');
            $table->float('patient_weight');
            $table->float('patient_height');
            $table->float('patient_abdominal_circumference');
            $table->dateTime('previous_appointment_date_start')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('appointments');
    }
}
