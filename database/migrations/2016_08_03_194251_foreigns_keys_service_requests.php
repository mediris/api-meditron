<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ForeignsKeysServiceRequests extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('service_requests', function (Blueprint $table) {

            $table->foreign('request_status_id')->references('id')->on('request_statuses')->onDelete('restrict');
            $table->foreign('patient_type_id')->references('id')->on('patient_types')->onDelete('restrict');
            $table->foreign('source_id')->references('id')->on('sources')->onDelete('restrict');
            $table->foreign('referring_id')->references('id')->on('referrings')->onDelete('restrict');
            $table->foreign('answer_id')->references('id')->on('answers')->onDelete('restrict');
            $table->foreign('pregnancy_status_id')->references('id')->on('pregnancy_statuses')->onDelete('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('service_requests', function (Blueprint $table) {
            $table->dropForeign('service_requests_request_status_id_foreign');
            $table->dropForeign('service_requests_patient_type_id_foreign');
            $table->dropForeign('service_requests_source_id_foreign');
            $table->dropForeign('service_requests_referring_id_foreign');
            $table->dropForeign('service_requests_answer_id_foreign');
            $table->dropForeign('service_requests_pregnancy_status_id_foreign');
        });
    }
}
