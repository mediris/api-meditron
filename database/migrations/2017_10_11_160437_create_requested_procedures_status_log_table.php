<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRequestedProceduresStatusLogTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('requested_procedures_status_log', function (Blueprint $table) {
            
                        $table->increments('id');
                        $table->integer('requested_procedure_id');
                        $table->integer('old_status');
                        $table->integer('new_status');
                        $table->dateTime('date');
                        $table->integer('user_id');
                        $table->timestamps();
            
                    });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('requested_procedures_status_log');
    }
}
