<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ForeignsKeysProcedureTemplate extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('procedure_template', function($table){

            $table->foreign('template_id')->references('id')->on('templates')->onDelete('cascade');
            $table->foreign('procedure_id')->references('id')->on('procedures')->onDelete('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('procedure_template', function(Blueprint $table){

            $table->dropForeign('procedure_template_template_id_foreign');
            $table->dropForeign('procedure_template_procedure_id_foreign');

        });
    }
}
