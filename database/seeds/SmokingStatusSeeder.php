<?php

use Illuminate\Database\Seeder;
use App\SmokingStatus;

class SmokingStatusSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $statuses = ['yes', 'no', 'unknown'];

        foreach($statuses as $status)
        {
            SmokingStatus::create([
                'name' => $status,
            ]);
        }
    }
}
