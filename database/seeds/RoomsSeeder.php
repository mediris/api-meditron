<?php

use Illuminate\Database\Seeder;
use App\Room;

use Maatwebsite\Excel\Facades\Excel;

class RoomsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        // se carga el seed a partir del archivo modalidad.csv
        Excel::load('database/exportFile/sala.csv', function($reader) {

            foreach ($reader->get() as $seed) {
                ///TO DO: antes del PAP se debe desincorporar el faker
                //$faker = Faker::create();
                // si el registro se encuentra activo
                if ($seed->activo == 1)
                    Room::create([
                        'name' => $seed->nombre,
                        'active' => $seed->activo,
                        'administrative_ID' => $seed->codigo_sala,
                        'division_id' => rand(1, 3),
                        'description' => $seed->descripcion,
                        'start_hour' => '07:00 AM',
                        'end_hour' => '06:00 PM',
                        'block_size' => 60,
                        'avoided_blocks' => 5,
                        //'quota' => rand(2, 4),
                        'quota' => 1,
                        'equipment_number' => $seed->numeroequipos,
                        'oldId' => $seed->id
                    ]);
            }
        });

    }
}
