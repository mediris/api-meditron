<?php

use Illuminate\Database\Seeder;
use App\Procedure;
//use Faker\Factory as Faker;

use Maatwebsite\Excel\Facades\Excel;

class ProceduresSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        // se carga el seed a partir del archivo procedimiento.csv
        Excel::load('database/exportFile/procedimiento.csv', function($reader) {

            foreach ($reader->get() as $seed) {
                ///TO DO: antes del PAP se debe desincorporar el faker
                //$faker = Faker::create();
                // si el registro se encuentra activo
                if ($seed->activo == 1) {
                    $procedure = Procedure::create([
                        'description' => $seed->descripcion,
                        'active' => $seed->activo,
                        'interview' => $seed->necesita_entrevista,
                        'mammography' => $seed->mamografia,
                        'worklist' => $seed->worklist,
                        'images' => 1,
                        'radiologist' => $seed->necesita_radiologo,
                        'radiologist_fee' => $seed->honorarioradiologo,
                        'technician' => $seed->necesitatecnico,
                        'technician_fee' => $seed->honorariotecnico,
                        'transcriptor_fee' => $seed->honorariotranscriptora,
                        'administrative_ID' => $seed->idadministrativo,
                        //'indications' => $faker->text(300),
                        'oldId' => $seed->id,
                        'modality_id' => $seed->id_modalidad
                    ]);

                    // JCH - 06/10/2017 - Se desincorpora por lectura al archivo CSV
                    // se llena la tabla pivote procedure_step
                    /*$procedure->steps()->attach([
                        rand(1, 5) => ['order' => 1],
                        rand(5, 10) => ['order' => 2],
                        rand(10, 15) => ['order' => 3],
                        rand(15, 20) => ['order' => 4],
                        rand(20, 25) => ['order' => 5],
                        rand(25, 30) => ['order' => 6],
                    ]);*/
                }
            }
        });
    }
}
