<?php

use Illuminate\Database\Seeder;
use App\Equipment;
use Maatwebsite\Excel\Facades\Excel;

class EquipmentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        // se carga el seed a partir del archivo equipo.csv
        Excel::load('database/exportFile/equipo.csv', function($reader) {

            foreach ($reader->get() as $seed) {
                if ($seed->activo == 1)
                    Equipment::create([
                        'ae_title' => $seed->aetitle,
                        'name' => $seed->nombre_dicom,
                        'active' => $seed->activo,
                        'oldId' => $seed->id
                    ]);
            }
        });


    }
}
