<?php

use Illuminate\Database\Seeder;
use App\RequestStatus;

class RequestStatusSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $statuses = ['created', 'scheduled', 'admitted', 'discharged'];

        foreach($statuses as $status)
        {
            RequestStatus::create([
                'name' => $status,
            ]);
        }
    }
}
