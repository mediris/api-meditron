<?php

use Illuminate\Database\Seeder;
use App\Answer;

class AnswersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $statuses = ['doctor', 'clinic', 'friends', 'patient', 'radio', 'press', 'magazines', 'tv', 'cinema', 'internet', 'social', 'other'];

        foreach($statuses as $status)
        {
            Answer::create([
              'active' => true,
              'description' => $status,
            ]);
        }
    }
}
