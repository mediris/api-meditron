<?php

/**
 * @fecha 15-06-2017
 * @programador Hendember Heras
 * @change: Se eliminó el registro con id cableado 999
 */

use Illuminate\Database\Seeder;
use App\BiRad;

class BiRadsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $values = ['BI-RADS 0',
                    'BI-RADS 1',
                    'BI-RADS 2', 
                    'BI-RADS 3',
                    'BI-RADS 4A',
                    'BI-RADS 4B',
                    'BI-RADS 4C',
                    'BI-RADS 5',
                    'BI-RADS 6'];

        foreach($values as $key => $value) {
            BiRad::create([
                'description' => $value,
                'frecuency'   => 0
            ]);
        }
    }
}
