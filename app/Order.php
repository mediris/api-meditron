<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    /**
     * @fecha: 25-11-2016
     * @programador: Juan Bigorra / Pascual Madrid
     * @objetivo: Campos que pueden ser llenados a través de eloquent (los que no salgan aquí no podrán ser llenados).
     */
    protected $fillable = [
        'name', 'administrative_ID', 'active',
    ];

    /**
     * @fecha: 25-11-2016
     * @programador: Juan Bigorra / Pascual Madrid
     * @objetivo: Relación: Un Order tiene muchos Procedures.
     */
    public function procedures()
    {
        return $this->belongsToMany(Procedure::class);
    }

    /**
     * @fecha: 25-11-2016
     * @programador: Juan Bigorra / Pascual Madrid
     * @objetivo: Función para cambiar el campo active de 0 a 1 y viceversa.
     */
    public function active()
    {
        $this->active = $this->active == 1 ? 0 : 1;
        $this->save();
    }
}
