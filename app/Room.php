<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Room extends Model
{
    /**
     * @fecha: 25-11-2016
     * @programador: Juan Bigorra / Pascual Madrid
     * @objetivo: Campos que pueden ser llenados a través de eloquent (los que no salgan aquí no podrán ser llenados).
     */
    protected $fillable = [
        'name', 'description', 'administrative_ID', 'active', 'division_id', 'start_hour', 'end_hour', 'block_size', 'avoided_blocks', 'quota', 'equipment_number'
    ];

    /**
     * @fecha: 25-11-2016
     * @programador: Juan Bigorra / Pascual Madrid
     * @objetivo: Relación: Un Rooms tiene muchos Equipment.
     */
    public function equipment()
    {
        return $this->hasMany(Equipment::class);
    }

    /**
     * @fecha: 25-11-2016
     * @programador: Juan Bigorra / Pascual Madrid
     * @objetivo: Relación: Un Rooms tiene un Division.
     */
    public function division()
    {
        return $this->belongsTo(Division::class);
    }

    /**
     * @fecha: 25-11-2016
     * @programador: Juan Bigorra / Pascual Madrid
     * @objetivo: Relación: Muchos Rooms tienen muchos Procedures.
     */
    public function procedures()
    {
        return $this->belongsToMany(Procedure::class)->withPivot('duration');
    }

    /**
     * @fecha: 25-11-2016
     * @programador: Juan Bigorra / Pascual Madrid
     * @objetivo: Relación: Un Room tiene muchos Appointments.
     */
    public function appointments()
    {
        return $this->hasMany(Appointment::class);
    }


    /**
     * @fecha: 25-11-2016
     * @programador: Juan Bigorra / Pascual Madrid
     * @objetivo: Función para cambiar el campo active de 0 a 1 y viceversa.
     */
    public function active()
    {
        $this->active = $this->active == 1 ? 0 : 1;
        $this->save();
    }
}
