<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Procedure;
use App\RequestedProceduresStatusLog;
use DB;

class RequestedProcedure extends Model
{
    /**
     * @fecha: 25-11-2016
     * @programador: Juan Bigorra / Pascual Madrid
     * @objetivo: Campos que pueden ser llenados a través de eloquent (los que no salgan aquí no podrán ser llenados).
     */
    protected $fillable = [
        'id',
        'service_request_id',
        'procedure_id',
        'study_instance_id',
        'comments',
        'urgent',
        'active',
        'requested_procedure_status_id',
        'print_label',
        'culmination_date',
        'study_number',
        'number_of_plates',
        'plates_size_id',
        'nurse',
        'deliver_results',
        'force_images',
        'blocked_status',
        'blocking_user_id',
        'approve_user_id',
        'bi_rad_id',
        'bi_rad_notified',
        'text',
        'technician_user_name',
        'radiologist_user_name',
        'transcriptor_user_name',
        'approve_user_name',
        'technician_user_id',
        'radiologist_user_id',
        'approve_user_id',
        'technician_end_date',
        'dictation_date',
        'dictation_file',
        'transcription_date',
        'approval_date',
        'suspension_date',
        'reject_user_id',
        'reject_date',
        'reject_reason',
        'reject_user_name',
        'transcriptor_user_id',
        'teaching_file',
        'teaching_file_text',
        'category_id',
        'sub_category_id',
        'suspension_reason_id',
        'suspension_user_id',
        'suspension_user_name',
        'equipment_id'
    ];

    /**
     * @fecha: 25-11-2016
     * @programador: Juan Bigorra / Pascual Madrid
     * @objetivo: Relación: Un RequestedProcedure tiene un ServiceRequest.
     */
    public function serviceRequest()
    {
        return $this->belongsTo(ServiceRequest::class);
    }

    /**
     * @fecha: 25-11-2016
     * @programador: Juan Bigorra / Pascual Madrid
     * @objetivo: Relación: Un RequestedProcedure tiene un Procedure.
     */
    public function procedure()
    {
        return $this->belongsTo(Procedure::class);
    }

    /**
     * @fecha: 14-08-2017
     * @programador: Hendember
     * @objetivo: Relación: Un RequestedProcedure tiene un Equipo.
     */
    public function equipment()
    {
        return $this->belongsTo(Equipment::class);
    }

    /**
     * @fecha: 25-11-2016
     * @programador: Juan Bigorra / Pascual Madrid
     * @objetivo: Relación: Un RequestedProcedure tiene un RequestedProcedureStatus. 1 => to-admit, 2 => to-do, 3 => to-dictate, 4 => to-transcribe, 5 => to-approve, 6 => finished, 7 => suspended
     */
    public function requestedProcedureStatus()
    {
        return $this->belongsTo(RequestedProcedureStatus::class);
    }

    /**
     * @fecha: 25-11-2016
     * @programador: Juan Bigorra / Pascual Madrid
     * @objetivo: Relación: Un RequestedProcedure tiene un PlatesSize.
     */
    public function platesSize()
    {
        return $this->belongsTo(PlatesSize::class);
    }

    /**
     * @fecha: 25-11-2016
     * @programador: Juan Bigorra / Pascual Madrid
     * @objetivo: Relación: Un RequestedProcedure tiene un BiRad.
     */
    public function biRad()
    {
        return $this->belongsTo(BiRad::class);
    }

    /**
     * @fecha: 25-11-2016
     * @programador: Juan Bigorra / Pascual Madrid
     * @objetivo: Relación: Un RequestedProcedure tiene muchos Addendums.
     */
    public function addendums()
    {
        return $this->hasMany(Addendums::class);
    }

    /**
     * @fecha: 25-11-2016
     * @programador: Juan Bigorra / Pascual Madrid
     * @objetivo: Relación: Un RequestedProcedure tiene muchos Delivers.
     */
    public function delivers()
    {
        return $this->hasMany(Deliver::class);
    }


    /**
     * @fecha: 25-11-2016
     * @programador: Juan Bigorra / Pascual Madrid
     * @objetivo: Función para cambiar el campo active de 0 a 1 y viceversa.
     */
    public function active()
    {

        $this->active = $this->active == 1 ? 0 : 1;


        return $this->save();

    }

    /**
     * @fecha: 25-11-2016
     * @programador: Juan Bigorra / Pascual Madrid
     * @objetivo: Función para bloquear estado de bloqueo y el nombre & id del usuario que bloqueó un RequestedProcedure.
     */
    public function lock($user_id, $role, $user_name = null)
    {
        //$role possibles are: 'technician', 'radiologist', 'transcriptor'

        switch($role)
        {
            case 'technician' :
                $this->technician_user_id = $user_id;
                $this->technician_user_name = $user_name;
                break;

            case 'radiologist' :
                $this->radiologist_user_id = $user_id;
                $this->radiologist_user_name = $user_name;
                break;

            case 'transcriptor' :
                $this->transcriptor_user_id = $user_id;
                $this->transcriptor_user_name = $user_name;
                break;
        }

        return $this->save();

    }

    public function setBlockParameters($user_id, $user_name, $option = null) {
        switch($option) {

            case 1:

                $this->blocked_status = 1;

                $this->blocking_user_id = $user_id;

                $this->blocking_user_name = $user_name;

                break;

            default:

                $this->blocked_status = $this->blocked_status == 1 ? 0 : 1;

                $this->blocked_status ? $this->blocking_user_id = $user_id : $this->blocking_user_id = 0;

                $this->blocked_status ? $this->blocking_user_name = $user_name : $this->blocking_user_name = '';

                break;
        }

        return $this->save();

    }

    public function isRequestedProcedureStatusUpdated ($requestedProcedure) {
        
        if ($this->requested_procedure_status_id == (int)$requestedProcedure['requested_procedure_status_id']) {

            return false;

        } else {

            return (int)$requestedProcedure['requested_procedure_status_id'];

        }

    }

    public function updateStatus ($newStatus) {


        switch ($newStatus) {

            case 1:

                $this->resetSuspension();
                $this->resetCulminationDate();
                $this->resetRejection();
                $this->resetApproval();
                $this->resetTranscription();
                $this->resetDictation();
                $this->resetTeachingFile();
                $this->resetBirad();
                $this->resetTechnician();
                $this->resetBlocking();
                $this->resetUrgentStatus();
                $this->resetBiopsyResult();

                $this->requested_procedure_status_id = $newStatus;

                break;

            case 2:

                $this->resetSuspension();
                $this->resetCulminationDate();
                $this->resetRejection();
                $this->resetApproval();
                $this->resetTranscription();
                $this->resetDictation();
                $this->resetTeachingFile();
                $this->resetBirad();
                $this->resetTechnician();
                $this->resetBlocking();
                $this->resetUrgentStatus();
                $this->resetBiopsyResult();

                $this->requested_procedure_status_id = $newStatus;

                break;

            case 3:

                $this->resetSuspension();
                $this->resetCulminationDate();
                $this->resetRejection();
                $this->resetApproval();
                $this->resetTranscription();
                $this->resetDictation();
                $this->resetTeachingFile();
                $this->resetBirad();
                $this->resetBiopsyResult();

                $this->requested_procedure_status_id = $newStatus;

                break;

            case 4:

                $this->resetSuspension();
                $this->resetCulminationDate();
                $this->resetRejection();
                $this->resetApproval();
                $this->resetTranscription();
                $this->resetBiopsyResult();

                $this->requested_procedure_status_id = $newStatus;

                break;

            case 5:

                $this->resetSuspension();
                $this->resetCulminationDate();
                $this->resetRejection();
                $this->resetApproval();
                $this->resetBiopsyResult();

                $this->requested_procedure_status_id = $newStatus;

                break;

            case 6:

                $this->resetCulminationDate();

                $this->requested_procedure_status_id = $newStatus;

                break;

            default:

                return false;

                break;

        }
    }

    public function resetTechnician () {

        $this->technician_user_id = 0;
        $this->technician_user_name = '';
        $this->technician_end_date = '0000-00-00 00:00:00';

        return true;
    }

    public function resetDictation () {

        $this->radiologist_user_id = 0;
        $this->radiologist_user_name = '';
        $this->text = '';
        $this->dictation_date = '0000-00-00 00:00:00';

        return true;
    }

    public function resetTranscription () {

        $this->transcriptor_user_id = 0;
        $this->transcriptor_user_name = '';
        $this->transcription_date = '0000-00-00 00:00:00';

        return true;
    }

    public function resetApproval () {

        $this->approve_user_id = 0;
        $this->approve_user_name = '';
        $this->approval_date = '0000-00-00 00:00:00';

        return true;
    }

    public function resetRejection () {

        $this->reject_user_id = 0;
        $this->reject_user_name = '';
        $this->reject_reason = '';
        $this->reject_date = '0000-00-00 00:00:00';

        return true;
    }

    public function resetBlocking () {

        $this->blocked_status = 0;
        $this->blocking_user_id = 0;
        $this->blocking_user_name = '';

        return true;
    }

    public function resetSuspension () {

        $this->suspension_user_id = 0;
        $this->suspension_user_name = '';
        $this->suspension_reason_id = NULL;
        $this->suspension_date = '0000-00-00 00:00:00';

        return true;
    }

    public function resetCulminationDate () {

        $this->culmination_date = '0000-00-00 00:00:00';

        return true;
    }

    public function resetBirad () {

        $this->bi_rad_id = null;

        return true;
    }

    public function resetBiopsyResult () {

        $this->biopsy_result = 0;

        return true;
    }

    public function resetTeachingFile () {

        $this->teaching_file = 0;
        $this->teaching_file_text = '';
        $this->category_id = 1;
        $this->sub_category_id = 1;

        return true;
    }

    public function resetUrgentStatus () {

        $this->urgent = 0;

        return true;
    }

    /*
     * @fecha: 14-08-2017
     * @programador: Hendember Heras
     * @objetivo: creates study instance (it's done after the insert, because we need the id)
     * study instance format: institution structure_base + equipment id + requested procedure id
     */
    public function setStudyInstance( $base ) {    
        if ( $this->equipment_id ) {
            $study_instance = $base . '.' . $this->equipment_id . '.' . $this->id;
            $this->update(['study_instance_id' => $study_instance]);    
        }

    }

    /*
     * @fecha: 14-08-2017
     * @programador: Hendember Heras
     * @objetivo: Writes the worklist to database
     */
    public function writeWorklist( $params ) {
        $this->load(['procedure', 'equipment', 'serviceRequest']);

        if ( $this->procedure->worklist ) {
            $this->serviceRequest->load(['referring']);
            $this->procedure->load(['modality']);

            $data['aetitle']                = $this->equipment->ae_title;
            $data['procsetpstartdate']      = date('Ymd');
            $data['procsetpstartdatetime']  = date('Ymd');
            $data['modality']               = $this->procedure->modality->name;
            $data['refp']                   = '';
            $data['psd']                    = $this->procedure->description;
            $data['psid']                   = $this->procedure->administrative_ID;
            $data['rpid']                   = $this->procedure->administrative_ID;
            $data['rpd']                    = $this->procedure->description;
            $data['siuid']                  = $this->study_instance_id;
            $data['an']                     = $this->id ;
            $data['rp']                     = $this->id;
            
	       if(!is_null($this->serviceRequest->referring)){
              $data['rpn']                    = $this->serviceRequest->referring->first_name . '^' . $this->serviceRequest->referring->last_name;
            }else{
              $data['rpn']                    = '';
            }

            $data['patn']                   = $this->serviceRequest->patient_first_name . '^' . $this->serviceRequest->patient_last_name;
            $data['pid']                    = $this->serviceRequest->patient_identification_id;

                                              //convert 2017-12-04 00:00:00 to 20171204
            $data['pabd']                   = substr( str_replace('-', '', $params['patient_birth_date']), 0, 8);
            $data['ps']                     = ($params['patient_sex']=='male')?'M':'F';

            $worklist = new Worklist($data);

            $worklist->save();
        }
    }

    public static function boot() {
        parent::boot();

        static::creating(function ($rp) {
            $result = self::getEquipment($rp->procedure_id);
            if ( count($result) > 0 ) {
                $rp->equipment_id = $result[0]->id;
            }
        });

        static::updating(function ($rp) {
            if($rp->getOriginal('requested_procedure_status_id') != $rp->requested_procedure_status_id) {
                $log = new RequestedProceduresStatusLog();
                $log->requested_procedure_id = $rp->id;
                $log->old_status = $rp->getOriginal('requested_procedure_status_id');
                $log->new_status = $rp->requested_procedure_status_id;
                $log->date = \DB::raw('NOW()');
                $log->user_id = $_REQUEST['user_id'];
                $log->save();
            }
        });
    }

    public static function getEquipment( $id ){
        // ubica todos los equipos asociados al procedimiento del Requested Procedure
        $equipments = Procedure::find( $id )->getEquipments();
        if ( count( $equipments ) > 0 ){

            //$rp->equipment_id = $equipments[0]->id;
            $eq = array();

            // extrae los id de los equipos
            foreach( $equipments as $equip )
                array_push($eq, $equip->id);

            $date1 = date('Y-m-d') . ' 00:00:00';
            $date2 = date('Y-m-d') . ' 23:59:59';
            $equip = implode(",", $eq);

            $result = DB::select(DB::raw("select e.id as 'id', e.name as 'name', count(rp.equipment_id) as 'cont' from equipment as e 
                            left join requested_procedures rp on e.id = rp.equipment_id 
                            and rp.created_at between '$date1' and '$date2'
                            where e.id in (" . $equip . ")
                            and e.active = 1
                            group by e.id, e.name
                            order by cont asc"));

            return $result;
        }
    }
}
