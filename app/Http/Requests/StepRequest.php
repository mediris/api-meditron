<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class StepRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'administrative_ID' => 'required|unique:steps|max:45',
            'description' => 'required|max:25',
            'indications' => 'required',
            'modality_id' => 'required'
        ];
    }
}
