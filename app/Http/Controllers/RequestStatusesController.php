<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\RequestStatus;
use Activity;
use Log;

class RequestStatusesController extends Controller
{
    public function index(Request $request)
    {
        try
        {
            $requestStatuses = RequestStatus::orderBy('name', 'asc')->get();
            return $requestStatuses;
        }
        catch(\Exception $e)
        {
            Log::useFiles(storage_path().'/logs/admin/admin.log');
            Log::alert('Error code: '.$e->getCode().' Error message: '.$e->getMessage().' Section: requestStatuses. Action: index');

            return response()->json(['error' => $e->getCode(), 'message' => $e->errorInfo[2]]);
        }
    }
}
