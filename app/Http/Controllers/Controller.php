<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesResources;
use Illuminate\Http\Request;
use App\Http\Requests;
use DB;

class Controller extends BaseController
{
    use AuthorizesRequests, AuthorizesResources, DispatchesJobs, ValidatesRequests;

    /**
     * @fecha: 25-11-2016
     * @programador: Juan Bigorra / Pascual Madrid
     * @objetivo: Función que retorna true o false dependiendo si un valor en un campo unique de una tabla está repetido.
     */
    public function unique(Request $request)
    {
        if($request->isMethod('post'))
        {
            if(isset($request->all()['id']))
            {
                return json_decode(DB::table($request->all()['table'])->where($request->all()['column'], $request->all()['value'])->where('id', '<>', $request->all()['id'])->count() > 0 ? 1 : 0);
            }
            else
            {
                return json_decode(DB::table($request->all()['table'])->where($request->all()['column'], $request->all()['value'])->count() > 0 ? 1 : 0);
            }
        }
    }
}
