<?php

    namespace App\Http\Controllers;

    use Activity;
    use App\ServiceRequest;
    use DataSourceResult;
    use DB;
    use Illuminate\Http\Request;
    use Log;
    use App\RequestedProcedure;
    use App\Procedure;
    use App\RequestDocument;

    class ServiceRequestsController extends Controller {

        public function __construct () {

            $host = config('database.connections.mysql.host');
            $dbname = config('database.connections.mysql.database');
            $conn = config('database.default');
            $username = config('database.connections.mysql.username');
            $password = config('database.connections.mysql.password');

            $this->DbHandler = new DataSourceResult("$conn:host=$host;dbname=$dbname", $username, $password);
        }

        public function index ( Request $request ) {
            try {
                if ( isset($request->all()['where']) ) {
                    $where = $request->all()['where'];
                    $serviceRequests = ServiceRequest::where($where)->orderBy('issue_date', 'desc')->get();
                } else {
                    $serviceRequests = ServiceRequest::all();
                }

                $serviceRequests->load([ 'source', 'requestStatus', 'smokingStatus', 'requestDocuments', 'answer', 'pregnancyStatus', 'patientType', 'referring', 'requestedProcedures' ]);

                foreach ($serviceRequests as $serviceRequest) {
                    $serviceRequest->requestedProcedures->load([ 'procedure', 'requestedProcedureStatus' ]);
                }

                return $serviceRequests;
            } catch (\Exception $e) {
                Log::useFiles(storage_path() . '/logs/reception/reception.log');
                Log::alert('Error code: ' . $e->getCode() . ' Error message: ' . $e->getMessage() . ' Section: Reception. Action: index');
                $errorMessage = isset( $e->errorInfo[2] ) ? $e->errorInfo[2] : $e->getMessage();

                return response()->json([ 'error' => $e->getCode(), 'message' => $errorMessage ]);
            }
        }

        public function show (ServiceRequest $serviceRequest, Request $request) {

            $serviceRequest->load([ 'source', 'requestStatus', 'smokingStatus', 'requestDocuments', 'answer', 'pregnancyStatus', 'patientType', 'referring', 'requestedProcedures' ]);
            /**
             * Log activity
             */
            Activity::log(trans('tracking.show', [ 'section' => 'reception', 'id' => $serviceRequest->id ]), $request->all()['user_id']);

            return $serviceRequest;
        }

        public function add (Request $request) {

            if ($request->isMethod('post')) {

                $this->validate($request, [
                    'patient_id'        => 'required',
                    'request_status_id' => 'required',
                    'patient_type_id'   => 'required',
                    'source_id'         => 'required',
                    'user_id'           => 'required',
                    'answer_id'         => 'required',
                    'weight'            => 'required|numeric',
                    'height'            => 'required|numeric',
                ]);

                try {
                    $serviceRequest = new ServiceRequest($request->all());
                    $serviceRequest->issue_date = date('Y-m-d H:i:s');

                    DB::transaction(function () use ($request, $serviceRequest) {

                        $serviceRequest->save();

                        /**
                         * Add the documents
                         */
                        if (isset( $request->all()['requestDocuments'] )) {
                            $serviceRequest->addRequestDocuments($request->all()['requestDocuments']);
                        }

                        /**
                         * Add the Requested Procedures
                         */
                        $serviceRequest->addRequestedProcedures($request->all()['requestedProcedures']);

                        /**
                         * Log activity
                         */
                        Activity::log(trans('tracking.create', [ 'section' => 'reception', 'id' => $serviceRequest->id ]), $request->all()['user_id']);
                    });
                } catch (\Exception $e) {
                    Log::useFiles(storage_path() . '/logs/reception/reception.log');
                    Log::alert('Error code: ' . $e->getCode() . ' Error message: ' . $e->getMessage() . ' Section: reception. Action: add');
                    $errorMessage = isset( $e->errorInfo[2] ) ? $e->errorInfo[2] : $e->getMessage();

                    return response()->json([ 'error' => $e->getCode(), 'message' => $errorMessage ]);
                }

                return response()->json([
                    'code'    => '201',
                    'message' => 'Created',
                    'newValue'      => $serviceRequest
                ]);
            }

        }

        public function documentAdd( Request $request, ServiceRequest $serviceRequest ) {
            $res = $serviceRequest->addRequestDocuments( [ $request ] );

            if ( isset( $res[0] ) ) {
                return response()->json([ 'code'     => '200',
                                          'message'  => 'Updated',
                                          'oldValue' => $res[0],
                                          'newValue' => $res[0] ]);
            } else {
                return response()->json([ 'error' => 304, 'message' => "not created" ]);
            }
        }

        public function documentShow( Request $request, RequestDocument $serviceRequestDocument ) {
            return $serviceRequestDocument;
        }

        public function edit (Request $request, ServiceRequest $serviceRequest) {

            if ($request->isMethod('post')) {
                $this->validate($request, [
                    'patient_id'        => 'required',
                    'request_status_id' => 'required',
                    'patient_type_id'   => 'required',
                    'source_id'         => 'required',
                    'user_id'           => 'required',
                    'answer_id'         => 'required',
                    'weight'            => 'required|numeric',
                    'height'            => 'required|numeric',
                    //'smoking_status_id' => 'required',
                    'patient_state_id'  => 'required',
                ]);

                $original = new ServiceRequest();
                foreach ($serviceRequest->getOriginal() as $key => $value) {
                    $original->$key = $value;
                }

                try {
                    DB::transaction(function () use ($request, $serviceRequest, $original) {

                        $serviceRequest->anonymous = 0;
                        $serviceRequest->update($request->all());

                        /**
                         * Add or edit the documents
                         */
                        if (isset( $request->all()['requestDocuments'] )) {
                            $serviceRequest->addRequestDocuments($request->all()['requestDocuments']);
                        }
                        else {
                            $serviceRequest->clearAllRequestDocuments();
                        }

                        /**
                         * Add or edit the Requested Procedures
                         */
                        if(isset($request->all()['requestedProcedures'])){
                            $serviceRequest->addRequestedProcedures($request->all()['requestedProcedures']);
                        }
                        /**
                         * Log activity
                         */
                        $serviceRequest->load([ 'source', 'requestStatus', 'smokingStatus', 'requestDocuments', 'answer', 'pregnancyStatus', 'patientType', 'referring', 'requestedProcedures' ]);
                        $original->load([ 'source', 'requestStatus', 'smokingStatus', 'requestDocuments', 'answer', 'pregnancyStatus', 'patientType', 'referring', 'requestedProcedures' ]);
                        Activity::log(trans('tracking.edit', [ 'section' => 'reception', 'id' => $serviceRequest->id, 'oldValue' => $original, 'newValue' => $serviceRequest ]),
                            $request->all()['user_id']);
                    });
                } catch (\Exception $e) {
                    Log::useFiles(storage_path() . '/logs/reception/reception.log');
                    Log::alert('Error code: ' . $e->getCode() . ' Error message: ' . $e->getMessage() . ' Section: reception. Action: edit');
                    $errorMessage = isset( $e->errorInfo[2] ) ? $e->errorInfo[2] : $e->getMessage();

                    return response()->json([ 'error' => $e->getCode(), 'message' => $errorMessage ]);
                }

                return response()->json([ 'code' => '200', 'message' => 'Updated', 'oldValue' => $original, 'newValue' => $serviceRequest ]);
            }
        }

        public function active (Request $request, ServiceRequest $serviceRequest) {

            try {
                $original = new ServiceRequest();
                foreach ($serviceRequest->getOriginal() as $key => $value) {
                    $original->$key = $value;
                }

                $serviceRequest->active();

                /**
                 * Log activity
                 */

                Activity::log(trans('tracking.edit', [ 'section' => 'reception', 'id' => $serviceRequest->id, 'oldValue' => $original, 'newValue' => $serviceRequest, 'action' => 'active' ]),
                    $request->all()['user_id']);

            } catch (\Exception $e) {
                Log::useFiles(storage_path() . '/logs/reception/reception.log');
                Log::alert('Error code: ' . $e->getCode() . ' Error message: ' . $e->getMessage() . ' Section: reception. Action: active');
                $errorMessage = isset( $e->errorInfo[2] ) ? $e->errorInfo[2] : $e->getMessage();

                return response()->json([ 'error' => $e->getCode(), 'message' => $errorMessage ]);
            }

            return response()->json([ 'code' => '200', 'message' => 'Updated', 'oldValue' => $original, 'newValue' => $serviceRequest ]);
        }

        public function lock (Request $request, ServiceRequest $serviceRequest, $option = NULL) {
            if ($request->isMethod('POST')) {

                try {
                    $today = new \DateTime();
                    $user_id = $request->all()['user_id'];
                    $userName = $request->all()['user_first_name'] . ' ' . $request->all()['user_last_name'];

                    $sr = ServiceRequest::where('patient_id', $serviceRequest->patient_id)->get();
                    foreach ($sr as $service_request) {
                        $serviceRequest = $service_request;
                        $original = new ServiceRequest();
                        foreach ($serviceRequest->getOriginal() as $key => $value) {
                            $original->$key = $value;
                        }
                    
                        //if(substr($original->issue_date, 0, strrpos($original->issue_date, ' ')) == $today->format('Y-m-d')){
                            foreach ($serviceRequest->getOriginal() as $key => $value) {
                                $original->$key = $value;
                            }

                            $original->load(['requestedProcedures'=> function ($query) {
                                $query->where('requested_procedure_status_id', 2);
                            }]);

                            $requestedProcedures = $original->requestedProcedures;

                            foreach ($requestedProcedures as $requestedProcedure) {
                                $requestedProcedure->lock($user_id, 'technician', $userName);
                                $requestedProcedure->setBlockParameters($user_id, $userName, $option);
                            }

                            $serviceRequest->load(['requestedProcedures'=> function ($query) {
                                $query->where('requested_procedure_status_id', 2);
                            }]);
                        //}
                    }

                    /**
                     * Log activity
                     */
                    Activity::log(trans('tracking.lock', [ 'section' => 'reception', 'id' => $serviceRequest->id, 'oldValue' => $original, 'newValue' => $serviceRequest, 'action' => 'lock' ]),
                        $user_id);

                } catch (\Exception $e) {

                    Log::useFiles(storage_path() . '/logs/reception/reception.log');
                    Log::alert('File: ' . $e->getFile() . ' Error Line: ' . $e->getLine() . ' Error code: ' . $e->getCode() . ' Error message: ' . $e->getMessage() . ' Section: reception. Action: lock');

                    $errorMessage = isset( $e->errorInfo[2] ) ? $e->errorInfo[2] : $e->getMessage();

                    return response()->json([ 'error' => $e->getCode(), 'message' => $errorMessage ]);

                }

                return response()->json([ 'code' => '200', 'message' => 'Updated', 'oldValue' => $original, 'newValue' => $serviceRequest ]);
            }
        }

        public function total (Request $request) {

            return ServiceRequest::all()->count();
        }
        public function approve(Request $request, ServiceRequest $serviceRequest){
            
            try{
                
                $requestedProcedures = RequestedProcedure::where('service_request_id', $serviceRequest->id)->get();
                
                foreach($requestedProcedures as $requestedProcedure){

                    $requestedProcedure->updateStatus(2);
                    
                    $requestedProcedure->save();
                        
                }
                return response()->json(['code' => '200', 'message' => 'Updated']);

            } catch(\Exception $e) {
                return response()->json(['code' => '200', 'error' => $e->getMessage()]);
            }
        }

    }
