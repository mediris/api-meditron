<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Category;
use App\Http\Requests;
use Activity;
use Log;

class CategoriesController extends Controller
{
    /**
     * @fecha: 25-11-2016
     * @programador: Juan Bigorra / Pascual Madrid
     * @objetivo: Retornar una colección en formato Json de Categories.
     */
    public function index(Request $request)
    {
        try
        {
            if ( isset($request->all()['where']) ) {
                $where = $request->all()['where'];
                $categories = Category::where($where)->get();
            } else {
                $categories = Category::all();
            }

            return $categories;
        }
        catch(\Exception $e)
        {
            Log::useFiles(storage_path().'/logs/admin/admin.log');
            Log::alert('Error code: '.$e->getCode().' Error message: '.$e->getMessage().' Section: categories. Action: index');

            return response()->json(['error' => $e->getCode(), 'message' => $e->getMessage()]);
        }
    }

    /**
     * @fecha: 25-11-2016
     * @programador: Juan Bigorra / Pascual Madrid
     * @objetivo: Retornar una instancia de Category.
     */
    public function show(Category $category, Request $request)
    {

        /**
         * Log activity
         */

        Activity::log(trans('tracking.show', ['section' => 'categories', 'id' => $category->id]), $request->all()['user_id']);


        return $category;
    }

    /**
     * @fecha: 25-11-2016
     * @programador: Juan Bigorra / Pascual Madrid
     * @objetivo: Función para agregar una nueva instancia de Category.
     */
    public function add(Request $request)
    {
        if($request->isMethod('post'))
        {
            $this->validate($request, [
                'description' => 'required|max:45',
                'language' => 'required|max:10',
            ]);

            $category = new Category($request->all());

            try
            {
                $category->save();

                /**
                 * Log activity
                 */

                Activity::log(trans('tracking.create', ['section' => 'categories', 'id' => $category->id]), $request->all()['user_id']);

                return response()->json(['code' => '201', 'message' => 'Created', 'id' => $category->id]);

            }
            catch(\Exception $e)
            {
                Log::useFiles(storage_path().'/logs/admin/admin.log');
                Log::alert('Error code: '.$e->getCode().' Error message: '.$e->getMessage().' Section: categories. Action: add');

                return response()->json(['error' => $e->getCode(), 'message' => $e->errorInfo[2]]);
            }

        }
        return response()->json(['error' => '400', 'message' => 'Bad Request']);
    }

    /**
     * @fecha: 25-11-2016
     * @programador: Juan Bigorra / Pascual Madrid
     * @objetivo: Función para editar una instancia de Category.
     */
    public function edit(Request $request, Category $category)
    {

        if($request->isMethod('post'))
        {
            $this->validate($request, [
                'description' => 'required|max:45',
                'language' => 'required|max:10',
            ]);

            $original = new Category();
            foreach($category->getOriginal() as $key => $value)
            {
                $original->$key = $value;
            }

            $category->active = 0;

            try
            {
                $category->update($request->all());


                /**
                 * Log activity
                 */

                Activity::log(trans('tracking.edit', ['section' => 'category', 'id' => $category->id, 'oldValue' => $original, 'newValue' => $category]), $request->all()['user_id']);

                return response()->json(['code' => '200', 'message' => 'Updated', 'oldValue' => $original, 'newValue' => $category]);


            }
            catch(\Exception $e)
            {
                Log::useFiles(storage_path().'/logs/admin/admin.log');
                Log::alert('Error code: '.$e->getCode().' Error message: '.$e->getMessage().' Section: categories. Action: edit');

                return response()->json(['error' => $e->getCode(), 'message' => $e->errorInfo[2]]);
            }

        }
        return $category;
    }

    /**
     * @fecha: 25-11-2016
     * @programador: Juan Bigorra / Pascual Madrid
     * @objetivo: Función para cambiar el estado de activo o no de un Category.
     */
    public function active(Request $request, Category $category)
    {
        try
        {

            $original = new Category();
            foreach($category->getOriginal() as $key => $value)
            {
                $original->$key = $value;
            }

            $category->active();

            /**
             * Log activity
             */

            Activity::log(trans('tracking.edit', ['section' => 'categories', 'id' => $category->id, 'oldValue' => $original, 'newValue' => $category, 'action' => 'active']), $request->all()['user_id']);

            return response()->json(['code' => '200', 'message' => 'Updated', 'oldValue' => $original, 'newValue' => $category]);

        }
        catch(\Exception $e)
        {
            Log::useFiles(storage_path().'/logs/admin/admin.log');
            Log::alert('Error code: '.$e->getCode().' Error message: '.$e->getMessage().' Section: categories. Action: active');
            return response()->json(['error' => $e->getCode(), 'message' => $e->errorInfo[2]]);
        }
        
    }

}
