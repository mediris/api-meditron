<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Patient;
use App\Http\Requests;
use Activity;
use Log;

class PatientsController extends Controller
{
    public function index()
    {
        try
        {
            $patients = Patient::all();
            return $patients;
        }
        catch(\Exception $e)
        {
            Log::useFiles(storage_path().'/logs/patients/patients.log');
            Log::alert('Error code: '.$e->getCode().' Error message: '.$e->getMessage().' Section: patients. Action: index');

            return response()->json(['error' => $e->getCode(), 'message' => $e->errorInfo[2]]);
        }
    }

    public function show(Patient $patient, Request $request)
    {
        /**
         * Log activity
         */

        Activity::log(trans('tracking.show', ['section' => 'patients', 'id' => $patient->id]), $request->all()['user_id']);

        $patient->load(['responsable', 'sex']);
        return $patient;
    }

    public function add(Request $request)
    {
        if($request->isMethod('post'))
        {
            
            $patient = new Patient($request->all());
            
            try
            {
                if($patient->save())
                {
                    /**
                     * Log activity
                     */

                    Activity::log(trans('tracking.create', ['section' => 'patients', 'id' => $patient->id]), $request->all()['user_id']);
                }
                else
                {
                    /**
                     * Log activity
                     */

                    Activity::log(trans('tracking.attempt', ['section' => 'patients', 'action' => 'create']), $request->all()['user_id']);

                }
            }
            catch(\Exception $e)
            {
                Log::useFiles(storage_path().'/logs/patients/patients.log');
                Log::alert('Error code: '.$e->getCode().' Error message: '.$e->getMessage().' Section: patients. Action: add');

                return response()->json(['error' => $e->getCode(), 'message' => $e->errorInfo[2]]);
            }
            return response()->json(['code' => '201', 'message' => 'Created', 'id' => $patient->id]);
        }
        return response()->json(['error' => '400', 'message' => 'Bad Request']);
    }

    public function edit(Request $request, Patient $patient)
    {
        if($request->isMethod('post'))
        {
            $this->validate($request, [
                'patient_ID' => 'required|max:45',
                'last_name' => 'required|max:25',
                'first_name' => 'required|max:25',
                'name_prefix' => 'required',
                'birth_date' => 'required|date',
                'occupation' => 'required|max:25',
                'address' => 'required|max:250',
                'country_of_residence' => 'required|max:25',
                'telephone_number' => 'required|max:45',
                'cellphone_number' => 'required|max:45',
                'email' => 'required|email',
                'citizenship' => 'required|max:25',
                'sex_id' => 'required',
            ]);

            $original = new Patient();
            foreach($patient->getOriginal() as $key => $value)
            {
                $original->$key = $value;
            }

            try
            {
               if($patient->update($request->all()))
               {
                   /**
                    * Log activity
                    */
                   Activity::log(trans('tracking.edit', ['section' => 'patients', 'id' => $patient->id, 'oldValue' => $original, 'newValue' => $patient]), $request->all()['user_id']);
               }
                else
                {
                    /**
                     * Log activity
                     */
                    Activity::log(trans('tracking.attempt-edit', ['id' => $patient->id, 'section' => 'patients', 'action' => 'edit']), $request->all()['user_id']);
                }
            }
            catch(\Exception $e)
            {
                Log::useFiles(storage_path().'/logs/patients/patients.log');
                Log::alert('Error code: '.$e->getCode().' Error message: '.$e->getMessage().' Section: patients. Action: edit');

                return response()->json(['error' => $e->getCode(), 'message' => $e->errorInfo[2]]);
            }

            return response()->json(['code' => '200', 'message' => 'Updated', 'oldValue' => $original, 'newValue' => $patient]);
        }
    }

    public function delete(Request $request, Patient $patient)
    {
        try
        {
            if($patient->delete())
            {
                /**
                 * Log activity
                 */
                Activity::log(trans('tracking.delete', ['id' => $patient->id, 'section' => 'patients']), $request->all()['user_id']);
            }
            else
            {
                /**
                 * Log activity
                 */
                Activity::log(trans('tracking.attempt-edit', ['id' => $patient->id, 'section' => 'patients', 'action' => 'delete']), $request->all()['user_id']);
            }

            return response()->json(['code' => '200', 'message' => 'Deleted']);
        }
        catch(\Exception $e)
        {
            Log::useFiles(storage_path().'/logs/patients/patients.log');
            Log::alert('Error code: '.$e->getCode().' Error message: '.$e->getMessage().' Section: patients. Action: delete');

            return response()->json(['error' => $e->getCode(), 'message' => $e->errorInfo[2]]);
        }
    }
}
