<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\RequestedProcedure;
use App\Addendums;
use Activity;
use Log;
use DB;
use DataSourceResult;

class RadiologistController extends Controller
{
    public function index(Request $request, $status = null)
    {
        try
        {
            
            $host = config('database.connections.mysql.host');
            $dbname = config('database.connections.mysql.database');
            $conn = config('database.default');
            $username = config('database.connections.mysql.username');
            $password = config('database.connections.mysql.password');

            $DbHandler = new DataSourceResult("$conn:host=$host;dbname=$dbname", $username, $password);

            $req = $request->all();

            $data = (object) $req['data'];

            if(isset($data->filter))
            {
                $data->filter = (object) $data->filter;
                foreach ($data->filter->filters as $key => $filter)
                {
                    $data->filter->filters[$key] = (object) $filter;
                }
            }

            if(isset($data->sort))
            {
                foreach ($data->sort as $key => $sortArray)
                {
                    $data->sort[$key] = (object) $sortArray;
                }
            }

            switch ($status) {
                case 3:
                    
                    $fields = ['patientTypeIcon','patientType','orderID','serviceRequestID','serviceIssueDate','procedureDescription','patientFirstName','patientLastName', 'blockedStatus', 'blockingUserId', 'blockingUserName', 'technicianUserName', 'radiologistUserName', 'transcriptorUserName', 'approveUserName', 'modality'];
                    $fields['patientID'] = ['type' => 'string'];    //This field needs a data type because with some values (14717687-2) it gets converted to a date
                    $result = $DbHandler->read('radiologist_to_dictate', $fields, $data);

                    break;

                case 4:

                    $fields = ['patientTypeIcon','patientType','orderID','serviceRequestID','serviceIssueDate','serviceDictationDate','procedureDescription','patientFirstName','patientLastName', 'blockedStatus', 'blockingUserId', 'blockingUserName', 'technicianUserName', 'radiologistUserName', 'transcriptorUserName', 'approveUserName', 'radiologistUserID'];
                    $fields['patientID'] = ['type' => 'string'];    //This field needs a data type because with some values (14717687-2) it gets converted to a date
                    $result = $DbHandler->read('radiologist_to_transcribe', $fields, $data);
                
                    break;

                case 5:
                    
                    $fields = ['patientTypeIcon','patientType','orderID','serviceRequestID','serviceIssueDate','procedureDescription','patientFirstName','patientLastName', 'blockedStatus', 'blockingUserId', 'blockingUserName', 'technicianUserName', 'radiologistUserName', 'transcriptorUserName', 'approveUserName', 'transcriptionDate'];
                    $fields['patientID'] = ['type' => 'string'];    //This field needs a data type because with some values (14717687-2) it gets converted to a date
                    $result = $DbHandler->read('radiologist_to_approve', $fields, $data);
                    
                    break;

                case 6:

                    $fields = ['patientTypeIcon','patientType','orderID','serviceRequestID','approvalDate','procedureDescription','patientFirstName','patientLastName', 'blockedStatus', 'blockingUserId', 'blockingUserName', 'technicianUserName', 'radiologistUserName', 'transcriptorUserName', 'approveUserName'];
                    $fields['patientID'] = ['type' => 'string'];    //This field needs a data type because with some values (14717687-2) it gets converted to a date
                    $result = $DbHandler->read('radiologist_addendum', $fields, $data);
                    
                    break;
                
                default:
                    # code...
                    break;
            }

            return response()->json(['code' => '200', 'data' => $result]);

        }
        catch(\Exception $e)
        {
            Log::useFiles(storage_path().'/logs/radiologist/radiologist.log');
            Log::alert('Error code: '.$e->getCode().' Error message: '.$e->getMessage().' Section: Radiologist. Action: index');
            $errorMessage = isset($e->errorInfo[2]) ? $e->errorInfo[2] : $e->getMessage();
            return response()->json(['error' => $e->getCode(), 'message' => $errorMessage]);
        }
    }

    public function datasource(Request $request, $status)
    {
        if($request->isMethod('post'))
        {
            try
            {
                $requestedProcedures = RequestedProcedure::where('requested_procedure_status_id', $status)->get();

                $requestedProcedures->load(['serviceRequest', 'procedure', 'requestedProcedureStatus', 'platesSize']);

                foreach($requestedProcedures as $requestedProcedure)
                {
                    $requestedProcedure->serviceRequest->load(['requestDocuments', 'source', 'patientType', 'requestStatus', 'smokingStatus', 'answer', 'pregnancyStatus', 'referring']);
                }

                return $requestedProcedures;
            }
            catch(\Exception $e)
            {
                Log::useFiles(storage_path().'/logs/radiologist/radiologist.log');
                Log::alert('Error code: '.$e->getCode().' Error message: '.$e->getMessage().' Section: Radiologist. Action: datasource');
                $errorMessage = isset($e->errorInfo[2]) ? $e->errorInfo[2] : $e->getMessage();
                return response()->json(['error' => $e->getCode(), 'message' => $errorMessage]);
            }
        }
    }

    public function show(RequestedProcedure $requestedProcedure, Request $request)
    {
        throw new \Exception( "This method is deprecated, use RequestedProcedureController::show instead" );
        
        /*Activity::log(trans('tracking.show', ['section' => 'requestedProcedures', 'id' => $requestedProcedure->id]), $request->all()['user_id']);

        $requestedProcedure->load(['serviceRequest', 'procedure', 'requestedProcedureStatus', 'platesSize']);
        
        $requestedProcedure->load(['addendums' => function ($query) use ($requestedProcedure){
            $query->where('requested_procedure_id', $requestedProcedure->id)
                     ->orderBy('id', 'desc')
                     ->limit(1);
        }]);

        $requestedProcedure->serviceRequest->load(['requestDocuments', 'source', 'patientType', 'requestStatus', 'smokingStatus', 'answer', 'pregnancyStatus', 'referring']);

        $requestedProcedure->procedure->load(['templates']);

        return $requestedProcedure;*/
    }

    public function edit(Request $request, RequestedProcedure $requestedProcedure)
    {
        
        throw new \Exception( "This method is deprecated, use RequestedProcedureController::edit instead" );
       /* try
        {

            $original = new RequestedProcedure();
            foreach($requestedProcedure->getOriginal() as $key => $value)
            {
                $original->$key = $value;
            }

            $requestedProcedure->update($request->all());

            Activity::log(trans('tracking.edit', ['section' => 'radiologist', 'id' => $requestedProcedure->id, 'oldValue' => $original, 'newValue' => $requestedProcedure, 'action' => 'edit']), $request->all()['user_id']);


        }
        catch(\Exception $e)
        {
            Log::useFiles(storage_path().'/logs/radiologist/radiologist.log');
            Log::alert('Error code: '.$e->getCode().' Error message: '.$e->getMessage().' Section: radiologist. Action: edit');
            $errorMessage = isset($e->errorInfo[2]) ? $e->errorInfo[2] : $e->getMessage();
            return response()->json(['error' => $e->getCode(), 'message' => $errorMessage]);
        }

        $requestedProcedure->load(['serviceRequest', 'procedure', 'requestedProcedureStatus', 'platesSize']);
        $original->load(['serviceRequest', 'procedure', 'requestedProcedureStatus', 'platesSize']);

        return response()->json(['code' => '200', 'message' => 'Updated', 'oldValue' => $original, 'newValue' => $requestedProcedure]);*/
    }

    public function active(Request $request, RequestedProcedure $requestedProcedure)
    {
        try
        {

            $original = new RequestedProcedure();
            foreach($requestedProcedure->getOriginal() as $key => $value)
            {
                $original->$key = $value;
            }

            $requestedProcedure->active();

            /**
             * Log activity
             */

            Activity::log(trans('tracking.edit', ['section' => 'radiologist', 'id' => $requestedProcedure->id, 'oldValue' => $original, 'newValue' => $requestedProcedure, 'action' => 'active']), $request->all()['user_id']);


        }
        catch(\Exception $e)
        {
            Log::useFiles(storage_path().'/logs/radiologist/radiologist.log');
            Log::alert('Error code: '.$e->getCode().' Error message: '.$e->getMessage().' Section: radiologist. Action: active');
            $errorMessage = isset($e->errorInfo[2]) ? $e->errorInfo[2] : $e->getMessage();
            return response()->json(['error' => $e->getCode(), 'message' => $errorMessage]);
        }

        $requestedProcedure->load(['serviceRequest', 'procedure']);
        $original->load(['serviceRequest', 'procedure']);

        return response()->json(['code' => '200', 'message' => 'Updated', 'oldValue' => $original, 'newValue' => $requestedProcedure]);

    }

    /**
     * @date: 14-11-2016
     * @programmer: Pascual Madrid
     * @description: Function to return the orders blocked by a given user
     */
    public function orders(Request $request)
    {
        if($request->isMethod('post'))
        {
            try
            {
                $requestedProcedures = RequestedProcedure::where('requested_procedure_status_id', 3)->where('blocked_status', 1)->where('blocking_user_id', $request->input('user_id'))->get();

                $requestedProcedures->load(['serviceRequest', 'procedure', 'requestedProcedureStatus', 'platesSize']);

                foreach($requestedProcedures as $requestedProcedure)
                {
                    $requestedProcedure->serviceRequest->load(['requestDocuments', 'source', 'patientType', 'requestStatus', 'smokingStatus', 'answer', 'pregnancyStatus', 'referring']);

                    $requestedProcedure->procedure->load(['templates']);
                }

                return $requestedProcedures;
            }
            catch(\Exception $e)
            {
                Log::useFiles(storage_path().'/logs/radiologist/radiologist.log');
                Log::alert('Error code: '.$e->getCode().' Error message: '.$e->getMessage().' Section: Radiologist. Action: orders');
                $errorMessage = isset($e->errorInfo[2]) ? $e->errorInfo[2] : $e->getMessage();
                return response()->json(['error' => $e->getCode(), 'message' => $errorMessage]);
            }
        }
    }
}
