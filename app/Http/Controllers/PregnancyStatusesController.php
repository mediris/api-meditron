<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\PregnancyStatus;
use Activity;
use Log;

class PregnancyStatusesController extends Controller
{
    /**
     * @fecha: 25-11-2016
     * @programador: Juan Bigorra / Pascual Madrid
     * @objetivo: Retornar una colección en formato Json de PregnancyStatuses.
     */
    public function index(Request $request)
    {
        try
        {
            $pregnancyStatuses = PregnancyStatus::orderBy('id', 'asc')->get();
            return $pregnancyStatuses;
        }
        catch(\Exception $e)
        {
            Log::useFiles(storage_path().'/logs/admin/admin.log');
            Log::alert('Error code: '.$e->getCode().' Error message: '.$e->getMessage().' Section: pregnancyStatuses. Action: index');

            return response()->json(['error' => $e->getCode(), 'message' => $e->errorInfo[2]]);
        }
    }
}
