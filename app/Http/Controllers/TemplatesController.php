<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Template;
use App\Procedure;
use App\Http\Requests;
use Activity;
use Log;


class TemplatesController extends Controller
{
    public function index()
    {
        try
        {
            $templates = Template::orderBy('description', 'asc')->get();
            return $templates;
        }
        catch(\Exception $e)
        {
            Log::useFiles(storage_path().'/logs/admin/admin.log');
            Log::alert('Error code: '.$e->getCode().' Error message: '.$e->getMessage().' Section: templates. Action: index');
            
            return response()->json(['error' => $e->getCode(), 'message' => $e->errorInfo[2]]);
        }
    }

    public function show(Template $template)
    {
        $template->load('procedures');
        return $template;
    }

    public function add(Request $request)
    {
        if($request->isMethod('post'))
        {
            $this->validate($request, [
                'description' => 'required|max:25',
            ]);

            $template = new Template($request->all());
            
            try
            {
                if($template->save())
                {   
                    if(isset($request->all()['procedure'])){
                        $template->procedures()->attach((int)$request->all()['procedure']);
                    }


                    /**
                     * Log activity
                     */

                    Activity::log(trans('tracking.create', ['section' => 'template', 'id' => $template->id]), $request->all()['user_id']);

                    $request->session()->flash('message', trans('messages.success-add', ['name' => trans('messages.template')]));
                    $request->session()->flash('class', 'alert alert-success');
                }
                else
                {
                    /**
                     * Log activity
                     */

                    Activity::log(trans('tracking.attempt', ['section' => 'template', 'action' => 'create']), $request->all()['user_id']);

                    $request->session()->flash('message', trans('messages.error-add', ['name' => trans('messages.template')]));
                    $request->session()->flash('class', 'alert alert-danger');
                }
            }
            catch(\Exception $e)
            {
                Log::useFiles(storage_path().'/logs/admin/admin.log');
                Log::alert('Error code: '.$e->getCode().' Error message: '.$e->getMessage().' Section: templates. Action: add');

                return response()->json(['error' => $e->getCode(), 'message' => $e->errorInfo[2]]);
            }

            return response()->json(['code' => '201', 'message' => 'Created', 'id' => $template->id]);
        }

        return response()->json(['error' => '400', 'message' => 'Bad Request']);
    }

    public function edit(Request $request, Template $template)
    {
        if($request->isMethod('post'))
        {
            $this->validate($request, [
                'description' => 'required|max:25',
            ]);

            $template->active = 0;

            $original = new Template();
            foreach($template->getOriginal() as $key => $value)
            {
                $original->$key = $value;
            }

            try
            {
                if($template->update($request->all()))
                {
                    /**
                     * Log activity
                     */

                    Activity::log(trans('tracking.edit', ['section' => 'template', 'id' => $template->id, 'oldValue' => $original, 'newValue' => $template]), $request->all()['user_id']);

                    $request->session()->flash('message', trans('messages.success-edit', ['name' => trans('messages.template')]));
                    $request->session()->flash('class', 'alert alert-success');
                }
                else
                {
                    /**
                     * Log activity
                     */

                    Activity::log(trans('tracking.attempt-edit', ['id' => $template->id, 'section' => 'template', 'action' => 'edit']), $request->all()['user_id']);

                    $request->session()->flash('message', trans('messages.error-edit', ['name' => trans('messages.template')]));
                    $request->session()->flash('class', 'alert alert-danger');
                }
            }
            catch(\Exception $e)
            {
                Log::useFiles(storage_path().'/logs/admin/admin.log');
                Log::alert('Error code: '.$e->getCode().' Error message: '.$e->getMessage().' Section: templates. Action: edit');

                return response()->json(['error' => $e->getCode(), 'message' => $e->errorInfo[2]]);
            }

            return response()->json(['code' => '200', 'message' => 'Updated', 'oldValue' => $original, 'newValue' => $template]);
        }

        $procedures = Procedure::all();
        return response()->json(['procedures' => $procedures, 'template' => $template]);
    }

    public function delete(Request $request, Template $template)
    {
        try
        {
            if($template->delete())
            {
                /**
                 * Log activity
                 */

                Activity::log(trans('tracking.delete', ['id' => $template->id, 'section' => 'source']), $request->all()['user_id']);

                $request->session()->flash('message', trans('messages.success-delete', ['name' => trans('messages.template')]));
                $request->session()->flash('class', 'alert alert-success');
            }
            else
            {
                /**
                 * Log activity
                 */

                Activity::log(trans('tracking.attempt-edit', ['id' => $template->id, 'section' => 'source', 'action' => 'delete']), $request->all()['user_id']);

                $request->session()->flash('message', trans('messages.error-delete', ['name' => trans('messages.template')]));
                $request->session()->flash('class', 'alert alert-danger');
            }
        }
        catch(\Exception $e)
        {
            Log::useFiles(storage_path().'/logs/admin/admin.log');
            Log::alert('Error code: '.$e->getCode().' Error message: '.$e->getMessage().' Section: templates. Action: delete');

            return response()->json(['error' => $e->getCode(), 'message' => $e->errorInfo[2]]);
        }

        return response()->json(['code' => '200', 'message' => 'Deleted']);
    }

    public function active(Request $request, Template $template)
    {
        try
        {
            $original = new Template();
            foreach($template->getOriginal() as $key => $value)
            {
                $original->$key = $value;
            }
            $template->active();

            /**
             * Log activity
             */

            Activity::log(trans('tracking.edit', ['section' => 'templates', 'id' => $template->id, 'oldValue' => $original, 'newValue' => $template, 'action' => 'active']), $request->all()['user_id']);

            $request->session()->flash('message', trans('alerts.success-edit'));
            $request->session()->flash('class', 'alert alert-success');

        }
        catch(\Exception $e)
        {
            Log::useFiles(storage_path().'/logs/admin/admin.log');
            Log::alert('Error code: '.$e->getCode().' Error message: '.$e->getMessage().' Section: templates. Action: active');
            return response()->json(['error' => $e->getCode(), 'message' => $e->errorInfo[2]]);
        }
        return response()->json(['code' => '200', 'message' => 'Updated', 'oldValue' => $original, 'newValue' => $template]);
    }
}
