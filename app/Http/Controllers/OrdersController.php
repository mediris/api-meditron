<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Order;
use App\Http\Requests;
use Activity;
use Log;

class OrdersController extends Controller
{
    /**
     * @fecha: 25-11-2016
     * @programador: Juan Bigorra / Pascual Madrid
     * @objetivo: Retornar una colección en formato Json de Orders.
     */
    public function index()
    {
        try
        {
            $orders = Order::all();
            return $orders;
        }
        catch(\Exception $e)
        {
            Log::useFiles(storage_path().'/logs/admin/admin.log');
            Log::alert('Error code: '.$e->getCode().' Error message: '.$e->getMessage().' Section: orders. Action: index');

            return response()->json(['error' => $e->getCode(), 'message' => $e->errorInfo[2]]);
        }
    }

    /**
     * @fecha: 25-11-2016
     * @programador: Juan Bigorra / Pascual Madrid
     * @objetivo: Retornar una instancia de Order.
     */
    public function show(Order $order, Request $request)
    {
        /**
         * Log activity
         */

        Activity::log(trans('tracking.show', ['section' => 'orders', 'id' => $order->id]), $request->all()['user_id']);

        $order->load('procedures');
        return $order;
    }

    /**
     * @fecha: 25-11-2016
     * @programador: Juan Bigorra / Pascual Madrid
     * @objetivo: Crear una nueva instancia de Order.
     */
    public function add(Request $request)
    {
        if($request->isMethod('post'))
        {
            $order = new Order($request->all());

            try
            {
                if($order->save())
                {
                    $order->procedures()->attach($request->input('procedures'));
                    /**
                     * Log activity
                     */

                    Activity::log(trans('tracking.create', ['section' => 'orders', 'id' => $order->id]), $request->all()['user_id']);

                    $request->session()->flash('message', trans('messages.success-add', ['name' => trans('messages.order')]));
                    $request->session()->flash('class', 'alert alert-success');
                }
                else
                {
                    /**
                     * Log activity
                     */

                    Activity::log(trans('tracking.attempt', ['section' => 'orders', 'action' => 'create']), $request->all()['user_id']);

                    $request->session()->flash('message', trans('messages.error-add', ['name' => trans('messages.order')]));
                    $request->session()->flash('class', 'alert alert-danger');
                }
            }
            catch(\Exception $e)
            {
                Log::useFiles(storage_path().'/logs/admin/admin.log');
                Log::alert('Error code: '.$e->getCode().' Error message: '.$e->getMessage().' Section: orders. Action: add');

                return response()->json(['error' => $e->getCode(), 'message' => $e->errorInfo[2]]);
            }

            return response()->json(['code' => '201', 'message' => 'Created', 'id' => $order->id]);
        }

        return response()->json(['error' => '400', 'message' => 'Bad Request']);
    }

    /**
     * @fecha: 25-11-2016
     * @programador: Juan Bigorra / Pascual Madrid
     * @objetivo: Editar una nueva instacia de Order.
     */
    public function edit(Request $request, Order $order)
    {
        if($request->isMethod('post'))
        {
            $order->procedures()->detach();
            $order->procedures()->attach($request->input('procedures'));

            $original = new Order();
            foreach($order->getOriginal() as $key => $value)
            {
                $original->$key = $value;
            }

            $order->active = 0;

            try
            {
                if($order->update($request->all()))
                {
                    /**
                     * Log activity
                     */

                    Activity::log(trans('tracking.edit', ['section' => 'orders', 'id' => $order->id, 'oldValue' => $original, 'newValue' => $order]), $request->all()['user_id']);

                    $request->session()->flash('message', trans('messages.success-edit', ['name' => trans('messages.order')]));
                    $request->session()->flash('class', 'alert alert-success');
                }
                else
                {
                    /**
                     * Log activity
                     */

                    Activity::log(trans('tracking.attempt-edit', ['id' => $order->id, 'section' => 'orders', 'action' => 'edit']), $request->all()['user_id']);

                    $request->session()->flash('message', trans('messages.error-edit', ['name' => trans('messages.order')]));
                    $request->session()->flash('class', 'alert alert-danger');
                }
            }
            catch(\Exception $e)
            {
                Log::useFiles(storage_path().'/logs/admin/admin.log');
                Log::alert('Error code: '.$e->getCode().' Error message: '.$e->getMessage().' Section: orders. Action: edit');

                return response()->json(['error' => $e->getCode(), 'message' => $e->errorInfo[2]]);
            }

            return response()->json(['code' => '200', 'message' => 'Updated', 'oldValue' => $original, 'newValue' => $order]);
        }

        return $order;
    }

    /**
     * @fecha: 25-11-2016
     * @programador: Juan Bigorra / Pascual Madrid
     * @objetivo: Eliminar una instancia de Order.
     */
    public function delete(Request $request, Order $order)
    {
        try
        {
            if($order->delete())
            {
                /**
                 * Log activity
                 */

                Activity::log(trans('tracking.delete', ['id' => $order->id, 'section' => 'order']), $request->all()['user_id']);

                $request->session()->flash('message', trans('messages.success-delete', ['name' => trans('messages.order')]));
                $request->session()->flash('class', 'alert alert-success');
            }
            else
            {
                /**
                 * Log activity
                 */

                Activity::log(trans('tracking.attempt-edit', ['id' => $order->id, 'section' => 'orders', 'action' => 'delete']), $request->all()['user_id']);

                $request->session()->flash('message', trans('messages.error-delete', ['name' => trans('messages.order')]));
                $request->session()->flash('class', 'alert alert-danger');
            }
            return response()->json(['code' => '200', 'message' => 'Deleted']);
        }
        catch(\Exception $e)
        {
            Log::useFiles(storage_path().'/logs/admin/admin.log');
            Log::alert('Error code: '.$e->getCode().' Error message: '.$e->getMessage().' Section: orders. Action: delete');

            return response()->json(['error' => $e->getCode(), 'message' => $e->errorInfo[2]]);
        }
    }

    /**
     * @fecha: 25-11-2016
     * @programador: Juan Bigorra / Pascual Madrid
     * @objetivo: Cambiar el estado de active de una instancia de Order.
     */
    public function active(Request $request, Order $order)
    {
        try
        {

            $original = new Order();
            foreach($order->getOriginal() as $key => $value)
            {
                $original->$key = $value;
            }
            $order->active();

            /**
             * Log activity
             */

            Activity::log(trans('tracking.edit', ['section' => 'orders', 'id' => $order->id, 'oldValue' => $original, 'newValue' => $order, 'action' => 'active']), $request->all()['user_id']);

            $request->session()->flash('message', trans('alerts.success-edit'));
            $request->session()->flash('class', 'alert alert-success');

        }
        catch(\Exception $e)
        {
            Log::useFiles(storage_path().'/logs/admin/admin.log');
            Log::alert('Error code: '.$e->getCode().' Error message: '.$e->getMessage().' Section: orders. Action: active');
            return response()->json(['error' => $e->getCode(), 'message' => $e->errorInfo[2]]);
        }
        return response()->json(['code' => '200', 'message' => 'Updated', 'oldValue' => $original, 'newValue' => $order]);
    }
}
