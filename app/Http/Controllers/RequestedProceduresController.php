<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\RequestedProcedure;
use App\Http\Requests;
use Activity;
use Log;
use DB;

class RequestedProceduresController extends Controller {

    public function index(Request $request) {

        try {

            if ( isset($request->all()['where']) ) {
                $where = $request->all()['where'];
                $requestedProcedures = RequestedProcedure::where($where)->get();
            } else {
                $requestedProcedures = RequestedProcedure::all();
            }

            return $requestedProcedures;
        } catch (\Exception $e) {
            Log::useFiles(storage_path() . '/logs/requestedProcedure/requestedProcedure.log');
            Log::alert('File: ' . $e->getFile() . ' Error code: ' . $e->getCode() . ' Error Line: ' . $e->getLine() . ' Error message: ' . $e->getMessage() . ' Section: requestedProcedures. Action: index');

            return response()->json(['error' => $e->getCode(), 'message' => $e->errorInfo[2]]);
        }
    }

    public function show(RequestedProcedure $requestedProcedure, Request $request) {

        /**
         * Log activity
         */

        Activity::log(trans('tracking.show', ['section' => 'requestedProcedures', 'id' => $requestedProcedure->id]), $request->all()['user_id']);

        $requestedProcedure->load(['serviceRequest', 'procedure', 'requestedProcedureStatus', 'platesSize', 'delivers']);
        
        $requestedProcedure->load(['addendums' => function ($query) use ($requestedProcedure){
            $query->where('requested_procedure_id', $requestedProcedure->id)
                     ->orderBy('id', 'desc')
                     ->limit(1);
        }]);

        $requestedProcedure->serviceRequest->load(['requestDocuments', 'source', 'patientType', 'requestStatus', 'smokingStatus', 'answer', 'pregnancyStatus', 'referring']);

        $requestedProcedure->procedure->load(['templates']);

        return $requestedProcedure;
    }

    /**
     * @fecha: 20-06-2017
     * @parametros: 
     * @programador: Hendember Heras
     * @objetivo: Devuelve todos los RequestedProcedures que: esten finalizados, tengan birad y el más reciente de cada paciente.
     */    
    public function showBirad(Request $request) {
        //$rp = RequestedProcedure::where('requested_procedure_status_id', $status)->get();
        $rp = RequestedProcedure::where('requested_procedure_status_id', 6)
                                ->where('bi_rad_notified', false)
                                ->whereNotNull('bi_rad_id')
                                ->get()
                                ->load('serviceRequest', 'biRad');

        return $rp;
    }

    public function edit(Request $request, RequestedProcedure $requestedProcedure) {

        /*if ($request->isMethod('post')) {
           
            $original = new RequestedProcedure();

            foreach ($requestedProcedure->getOriginal() as $key => $value) {

                $original->$key = $value;

            }

            try {
                DB::transaction(function () use ($request, $requestedProcedure, $original) {

                    $data = $request->all()['data'];

                    $requestedProcedure->update($data);

                    Activity::log(trans('tracking.edit', ['section' => 'requestedProcedures', 'id' => $requestedProcedure->id, 'oldValue' => $original, 'newValue' => $requestedProcedure]), $request->all()['user_id']);
                });
            } catch (\Exception $e) {
                Log::useFiles(storage_path() . '/logs/requestedProcedures/requestedProcedures.log');
                Log::alert('File: ' . $e->getFile() . ' Error code: ' . $e->getCode() . ' Error Line: ' . $e->getLine() . ' Error message: ' . $e->getMessage() . ' Section: requestedProcedures. Action: edit');
                $errorMessage = isset($e->errorInfo[2]) ? $e->errorInfo[2] : $e->getMessage();
                return response()->json(['error' => $e->getCode(), 'message' => $errorMessage]);
            }

            return response()->json(['code' => '200', 'message' => 'Updated', 'oldValue' => $original, 'newValue' => $requestedProcedure]);
        }*/
        //el código anterior es lo que estaba, el código siguiente se copió de RadiologistController::edit


         try
        {

            $original = new RequestedProcedure();
            foreach($requestedProcedure->getOriginal() as $key => $value)
            {
                $original->$key = $value;
            }
            
            if(isset($request->all()['requested_procedure_status_id']) && $status = $requestedProcedure->isRequestedProcedureStatusUpdated($request->all())) {
                
                $requestedProcedure->updateStatus($status);

            }
            
            
            $requestedProcedure->update($request->all());

            Activity::log(trans('tracking.edit', ['section' => 'radiologist', 'id' => $requestedProcedure->id, 'oldValue' => $original, 'newValue' => $requestedProcedure, 'action' => 'edit']), $request->all()['user_id']);


        }
        catch(\Exception $e)
        {
            Log::useFiles(storage_path().'/logs/radiologist/radiologist.log');
            Log::alert('Error code: '.$e->getCode().' Error message: '.$e->getMessage().' Section: radiologist. Action: edit');
            $errorMessage = isset($e->errorInfo[2]) ? $e->errorInfo[2] : $e->getMessage();
            return response()->json(['error' => $e->getCode(), 'message' => $errorMessage]);
        }

        $requestedProcedure->load(['serviceRequest', 'procedure', 'requestedProcedureStatus', 'platesSize']);
        $requestedProcedure->serviceRequest->load(['requestDocuments', 'source', 'patientType', 'requestStatus', 'smokingStatus', 'answer', 'pregnancyStatus', 'referring']);
        $original->load(['serviceRequest', 'procedure', 'requestedProcedureStatus', 'platesSize']);

        return response()->json(['code' => '200', 'message' => 'Updated', 'oldValue' => $original, 'newValue' => $requestedProcedure]);
    }

    public function active(Request $request, RequestedProcedure $requestedProcedure, $status = null) {

        if ($request->isMethod('POST')) {

            try {

                $user_id = $request->all()['user_id'];

                $original = new RequestedProcedure();

                foreach ($requestedProcedure->getOriginal() as $key => $value) {

                    $original->$key = $value;

                }

                isset($status) ? $requestedProcedure->requested_procedure_status_id = $status : '';


                isset($status) ? $requestedProcedure->update($requestedProcedure->toArray()) : $requestedProcedure->active();

                /**
                 * Log activity
                 */

                $requestedProcedure->load(['requestedProcedureStatus', 'serviceRequest', 'procedure', 'platesSize']);
                $original->load(['requestedProcedureStatus', 'serviceRequest', 'procedure', 'platesSize']);

                Activity::log(trans('tracking.active', ['section' => 'requestedProcedures', 'id' => $requestedProcedure->id, 'oldValue' => $original, 'newValue' => $requestedProcedure, 'action' => 'active']), $user_id);

                return response()->json(['code' => '200', 'message' => 'Updated', 'oldValue' => $original, 'newValue' => $requestedProcedure]);

            } catch (\Exception $e) {
                Log::useFiles(storage_path() . '/logs/requestedprocedures/requestedprocedures.log');
                Log::alert('File: ' . $e->getFile() . ' Error code: ' . $e->getCode() . ' Error Line: ' . $e->getLine() . ' Error message: ' . $e->getMessage() . ' Section: requestedprocedures. Action: active');
                $errorMessage = isset($e->errorInfo[2]) ? $e->errorInfo[2] : $e->getMessage();
                return response()->json(['error' => $e->getCode(), 'message' => $errorMessage]);
            }
        }
    }    

    public function lock(Request $request, RequestedProcedure $requestedProcedure, $role, $option = null) {

//        dd($role,$option);
        if ($request->isMethod('post')) {

            try {

                $user_id = $request->all()['user_id'];
                $user_name = $request->all()['user_first_name'] . ' ' . $request->all()['user_last_name'];

                $original = new RequestedProcedure();

                foreach ($requestedProcedure->getOriginal() as $key => $value) {

                    $original->$key = $value;

                }

                $requestedProcedure->lock($user_id, $role, $user_name);
                $requestedProcedure->setBlockParameters($user_id, $user_name, $option);


                /**
                 * Log activity
                 */

                Activity::log(trans('tracking.edit', ['section' => 'requestedProcedures', 'id' => $requestedProcedure->id, 'oldValue' => $original, 'newValue' => $requestedProcedure]), $request->all()['user_id']);

                return response()->json(['code' => '200', 'message' => 'Updated', 'oldValue' => $original, 'newValue' => $requestedProcedure]);

            } catch (\Exception $e) {
                Log::useFiles(storage_path() . '/logs/requestedProcedures/requestedProcedures.log');
                Log::alert('File: ' . $e->getFile() . ' Error code: ' . $e->getCode() . ' Error Line: ' . $e->getLine() . ' Error message: ' . $e->getMessage() . ' Section: requestedProcedures. Action: lock');
                $errorMessage = isset($e->errorInfo[2]) ? $e->errorInfo[2] : $e->getMessage();
                return response()->json(['error' => $e->getCode(), 'message' => $errorMessage]);
            }
        }
    }

    public function total(Request $request, $status) {

        return RequestedProcedure::where('requested_procedure_status_id', $status)->get()->count();
    }

    public function totalByMonth(Request $request, $status) {

        return RequestedProcedure::where('requested_procedure_status_id', $status)->whereYear('created_at', '=', date('Y'))->whereMonth('created_at', '=', date('m'))->get()->count();
    }

    public function totalByUser(Request $request, $status, $user_id) {

        switch ($status) {
            case 3:
                return RequestedProcedure::where('technician_user_id', $user_id)->get()->count();
                break;

            case 4:
                return RequestedProcedure::where('radiologist_user_id', $user_id)->get()->count();
                break;

            case 5:
                return RequestedProcedure::where('transcriptor_user_id', $user_id)->get()->count();
                break;

            case 6:
                return RequestedProcedure::where('approve_user_id', $user_id)->get()->count();
                break;
        }
    }

    public function writeworklist(Request $request, RequestedProcedure $requestedProcedure) {
        if($request->isMethod('post')) {
            
            try {
                $data['patient_birth_date'] = $request->patient_birth_date;
                $data['patient_sex'] = $request->patient_sex;
                
                $requestedProcedure->writeWorklist( $data );
                
                return response()->json(['code' => '200', 'message' => 'Writed', 'id' => $requestedProcedure->id]);
            }
            catch(\Exception $e) {
                Log::useFiles(storage_path().'/logs/requestedProcedures/requestedProcedures.log');
                Log::alert('File: ' . $e->getFile() . ' Error Line: ' . $e->getLine() . ' Error code: '.$e->getCode().' Error message: '.$e->getMessage(). ' Section: requestedProcedures. Action: write');
                $errorMessage = isset($e->errorInfo[2]) ? $e->errorInfo[2] : $e->getMessage();
                return response()->json(['error' => $e->getCode(), 'message' => $errorMessage]);
            }
        }
    }

    public function setStudyInstance( Request $request, RequestedProcedure $requestedProcedure ) {
        try {            
            $requestedProcedure->setStudyInstance( $request->base ); 
            
            return response()->json(['code' => '200', 'message' => 'Writed', 'id' => $requestedProcedure->id]);
        }
        catch(\Exception $e) {
            Log::useFiles(storage_path().'/logs/requestedProcedures/requestedProcedures.log');
            Log::alert('File: ' . $e->getFile() . ' Error Line: ' . $e->getLine() . ' Error code: '.$e->getCode().' Error message: '.$e->getMessage(). ' Section: requestedProcedures. Action: StudyInstance');
            $errorMessage = isset($e->errorInfo[2]) ? $e->errorInfo[2] : $e->getMessage();
            return response()->json(['error' => $e->getCode(), 'message' => $errorMessage]);
        }
    }
}
