<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class NotificationTemplate extends Model
{
    protected $fillable = [
        'description', 'active', 'template',
    ];

    public function active() {
        $this->active = $this->active == 1 ? 0 : 1;
        $this->save();
    }
}
