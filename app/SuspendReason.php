<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SuspendReason extends Model
{
    protected $fillable = [
        'description', 'active', 'admin_reason'
    ];

    public function active() {
        $this->active = $this->active == 1 ? 0 : 1;
        $this->save();
    }
}