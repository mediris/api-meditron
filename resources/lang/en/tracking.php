<?php

return [


    /*
    |--------------------------------------------------------------------------
    | Tracking Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during the user tracking action.
    |
    */

    'create' => ':section with id: :id was created.',
    'show' => ':section with id: :id was showed.',
    'edit' => ':section with id: :id was edit. 
                Old value: :oldValue
                New value: :newValue',
    'delete' => ':section with id: :id was deleted.',

    'attempt' => 'attempt to :action :section without success',

    'attempt-edit' => 'attempt to :action :section with id: :id without success',

];