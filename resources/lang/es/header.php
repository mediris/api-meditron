<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Header Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during
    |
    */

    'home' => 'Inicio',
    'login' => 'Entrar',
    'register' => 'Registrar',
    'welcome' => 'Hola :prefix :user',
    'logout' => 'Salir',

];
