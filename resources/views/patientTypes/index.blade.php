@extends('layouts.app')

@section('content')

    <div class="container">

        <h1>{{ ucfirst(trans('messages.patient-types')) }}</h1>

        @if(Session::has('message'))
            <div class="{{ Session::get('class') }}">
                <p>{{ Session::get('message') }}</p>
            </div>
        @endif

        <a href="{{ route('patientTypes.add') }}">{{ ucfirst(trans('messages.add')) }} +</a>

        <table class="table table-bordered">
            <tr>
                <th>Id</th>
                <th>Description</th>
                <th>Administrative Id</th>
                <th>Actions</th>
            </tr>
            @foreach($patientTypes as $patientType)
                <tr>
                    <td>{{ $patientType->id }}</td>
                    <td>{{ $patientType->description }}</td>
                    <td>{{ $patientType->administrative_id }}</td>
                    <td><a href="{{ route('patientTypes.show', [$patientType]) }}">{{ ucfirst(trans('messages.show')) }}</a> | <a href="{{ route('patientTypes.edit', [$patientType]) }}">{{ ucfirst(trans('messages.edit')) }}</a> | <a href="{{ route('patientTypes.delete', [$patientType]) }}" onclick="return confirm('{{ trans("patientTypes.delete") }}')">{{ ucfirst(trans('messages.purge')) }}</a></td>
                </tr>
            @endforeach
        </table>

    </div>


@endsection