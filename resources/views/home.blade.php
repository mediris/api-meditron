@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">Dashboard</div>

                <div class="panel-body">
                    <h1>Sections of the Api</h1>
                </div>

                <table class="table table-bordered">
                    <thead>
                        <tr><th>Link</th></tr>

                    </thead>
                    <tbody>
                        <tr><td><a href="{{route('users')}}">Users</a></td></tr>
                        <tr><td><a href="{{route('institutions')}}">Institutions</a></td></tr>
                        <tr><td><a href="{{route('rooms')}}">Rooms</a></td></tr>
                        <tr><td><a href="{{route('divisions')}}">Divisions</a></td></tr>
                        <tr><td><a href="{{route('equipment')}}">Equipment</a></td></tr>
                        <tr><td><a href="{{route('modalities')}}">Modalities</a></td></tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection
