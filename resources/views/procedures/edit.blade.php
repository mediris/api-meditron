@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">{{ trans('messages.procedure') }}</div>
                    <div class="panel-body">
                        <form class="form-horizontal" role="form" method="POST" action="{{ route('procedures.edit', [$procedure]) }}">
                            {!! csrf_field() !!}


                            <div class="form-group{{ $errors->has('description') ? ' has-error' : '' }}">
                                <label class="col-md-4 control-label">Description</label>

                                <div class="col-md-6">
                                    <input type="text" class="form-control" name="description" value="{{ $procedure->description }}">

                                    @if ($errors->has('description'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('description') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>


                            <div class="form-group{{ $errors->has('active') ? ' has-error' : '' }}">
                                <label class="col-md-4 control-label">Is Active?</label>

                                <div class="col-md-6">
                                    <input type="checkbox" class="form-control" name="active" value="1" {{ $procedure->active == 1 ? 'checked' : '' }}>

                                    @if ($errors->has('active'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('active') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('interview') ? ' has-error' : '' }}">
                                <label class="col-md-4 control-label">Needs Interview?</label>

                                <div class="col-md-6">
                                    <input type="checkbox" class="form-control" name="interview" value="1" {{ $procedure->interview == 1 ? 'checked' : '' }}>

                                    @if ($errors->has('interview'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('interview') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('mammography') ? ' has-error' : '' }}">
                                <label class="col-md-4 control-label">Mammography</label>

                                <div class="col-md-6">
                                    <input type="checkbox" class="form-control" name="mammography" value="1" {{ $procedure->mammography == 1 ? 'checked' : '' }}>

                                    @if ($errors->has('mammography'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('mammography') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('worklist') ? ' has-error' : '' }}">
                                <label class="col-md-4 control-label">Worklist</label>

                                <div class="col-md-6">
                                    <input type="checkbox" class="form-control" name="worklist" value="1" {{ $procedure->worklist == 1 ? 'checked' : '' }}>

                                    @if ($errors->has('worklist'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('worklist') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('images') ? ' has-error' : '' }}">
                                <label class="col-md-4 control-label">Images</label>

                                <div class="col-md-6">
                                    <input type="checkbox" class="form-control" name="images" value="1" {{ $procedure->images == 1 ? 'checked' : '' }}>

                                    @if ($errors->has('images'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('images') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('radiologist') ? ' has-error' : '' }}">
                                <label class="col-md-4 control-label">Radiologist</label>

                                <div class="col-md-6">
                                    <input type="checkbox" class="form-control" name="radiologist" value="1" {{ $procedure->radiologist == 1 ? 'checked' : '' }}>

                                    @if ($errors->has('radiologist'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('radiologist') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('radiologist_fee') ? ' has-error' : '' }}">
                                <label class="col-md-4 control-label">Radiologist Fee</label>

                                <div class="col-md-6">
                                    <input type="number" step="0.1" class="form-control" name="radiologist_fee" value="{{ $procedure->radiologist_fee }}">

                                    @if ($errors->has('radiologist_fee'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('radiologist_fee') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>



                            <div class="form-group{{ $errors->has('technician') ? ' has-error' : '' }}">
                                <label class="col-md-4 control-label">Technician</label>

                                <div class="col-md-6">
                                    <input type="checkbox" class="form-control" name="technician" value="1" {{ $procedure->technician == 1 ? 'checked' : '' }}>

                                    @if ($errors->has('technician'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('technician') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('technician_fee') ? ' has-error' : '' }}">
                                <label class="col-md-4 control-label">Technician Fee</label>

                                <div class="col-md-6">
                                    <input type="number" step="0.1" class="form-control" name="technician_fee" value="{{ $procedure->technician_fee }}">

                                    @if ($errors->has('technician_fee'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('technician_fee') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('transcriptor_fee') ? ' has-error' : '' }}">
                                <label class="col-md-4 control-label">Transcriptor Fee</label>

                                <div class="col-md-6">
                                    <input type="number" step="0.1" class="form-control" name="transcriptor_fee" value="{{ $procedure->transcriptor_fee }}">

                                    @if ($errors->has('transcriptor_fee'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('transcriptor_fee') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('modality_id') ? ' has-error' : '' }}">
                                <label class="col-md-4 control-label">modality</label>

                                <div class="col-md-6">
                                    <select class="form-control" name="modality_id">
                                        <option value="">Select</option>
                                        @foreach($modalities as $modality)
                                            @if($procedure->modality_id == $modality->id)
                                                <option value="{{ $modality->id }}" selected>{{ $modality->name }}</option>
                                            @else
                                                <option value="{{ $modality->id }}">{{ $modality->name }}</option>
                                            @endif
                                        @endforeach
                                    </select>

                                    @if ($errors->has('modality_id'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('modality_id') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('administrative_ID') ? ' has-error' : '' }}">
                                <label class="col-md-4 control-label">Administrative ID</label>

                                <div class="col-md-6">
                                    <input type="text" class="form-control" name="administrative_ID" value="{{ $procedure->administrative_ID }}">

                                    @if ($errors->has('administrative_ID'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('administrative_ID') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('indications') ? ' has-error' : '' }}">
                                <label class="col-md-4 control-label">indications</label>

                                <div class="col-md-6">
                                    <textarea class="form-control" name="indications">{{ $procedure->indications }}</textarea>

                                    @if ($errors->has('indications'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('indications') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('equipment') ? ' has-error' : '' }}">
                                <label class="col-md-4 control-label">Equipment</label>

                                <div class="col-md-6">
                                    <select class="form-control" name="equipment[]" multiple>
                                        @foreach($equipment as $equipment)
                                            @if($procedure->hasEquipment($equipment->id))
                                                <option value="{{ $equipment->id }}" selected>{{ $equipment->name }}</option>
                                            @else
                                                <option value="{{ $equipment->id }}">{{ $equipment->name }}</option>
                                            @endif
                                        @endforeach
                                    </select>

                                    @if ($errors->has('equipment'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('equipment') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('consumables') ? ' has-error' : '' }}">
                                <label class="col-md-4 control-label">consumables</label>

                                <div class="col-md-6">
                                    <select class="form-control" name="consumables[]" multiple>
                                        @foreach($consumables as $consumable)
                                            @if($procedure->hasConsumable($consumable->id))
                                                <option value="{{ $consumable->id }}" selected>{{ $consumable->description }}</option>
                                            @else
                                                <option value="{{ $consumable->id }}">{{ $consumable->description }}</option>
                                            @endif
                                        @endforeach
                                    </select>

                                    @if ($errors->has('consumables'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('consumables') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>


                            <div class="form-group">
                                <div class="col-md-6 col-md-offset-4">
                                    <button type="submit" class="btn btn-primary">
                                        {{ ucfirst(trans('messages.edit')) }}
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
