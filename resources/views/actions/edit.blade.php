@extends('layouts.app')

@section('content')

    <style>

        ul
        {
            margin-left:40px;
        }

    </style>

    <div class="container">

        <div class="row">
            <h1>Edit an Action</h1>
        </div>

        <div class="row">

            <form class="form-horizontal col-md-8" action="{{ route('actions.edit', [$action]) }}" method="post">

                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <input type="hidden" name="id" value="{{ $action->id }}">

                <div class="form-group">
                    <label for="name" class="col-sm-2 control-label">Action Name</label>

                    <div class="col-sm-10">
                        <input type="text" class="form-control" name="name" placeholder="Action Name" value="{{ $action->name }}">
                    </div>

                </div>

                <!--<div class="form-group">
                    <label for="section_id" class="col-sm-2 control-label">Section</label>

                    <div class="col-sm-10">
                        <select class="form-control" name="section_id" val="{{ $action->section->id }}">

                            <option value="">Select</option>

                            @foreach($sections as $section)
                                @if($section->id == $action->section_id)
                                    <option selected value="{{ $section->id }}">{{ $section->name }}</option>
                                @else
                                    <option value="{{ $section->id }}">{{ $section->name }}</option>
                                @endif
                            @endforeach

                        </select>
                    </div>

                </div>-->

                <div class="form-group">

                    <label for="parent_id" class="col-sm-2 control-label">Section</label>

                    <div class="col-sm-10">

                        <ul>
                            @foreach($roots as $root)
                                <li>

                                    <div class="radio">
                                        <label><input type="radio" name="section_id" value="{{ $root->id }}" {{ $root->id == $action->section_id ? 'checked' : '' }}>{{ $root->name }}</label>
                                    </div>

                                    {{ $root->printChildsActions($action->section_id) }}
                                </li>
                            @endforeach

                        </ul>

                    </div>

                </div>

                <div class="form-group">
                    <div class="col-sm-offset-2 col-sm-10">
                        <button type="submit" class="btn btn-default">Submit</button>
                    </div>
                </div>

            </form>

        </div>

    </div>

@endsection