@extends('layouts.app')

@section('content')

    <div class="container">

        <h1>Configurations</h1>

        @if(Session::has('message'))
            <div class="{{ Session::get('class') }}">
                <p>{{ Session::get('message') }}</p>
            </div>
        @endif


        <!--<a href="{{ route('divisions.add') }}">Add new +</a>-->

        <table class="table table-bordered">
            <tr>
                <th>Id</th>
                <th>Institution</th>
                <th>Actions</th>
            </tr>
            @foreach($configurations as $configuration)
                <tr>
                    <td>{{ $configuration->id }}</td>
                    <td>{{ $configuration->institution->name }}</td>
                    <td><a href="{{ route('configurations.show', [$configuration]) }}">Show</a> | <a href="{{ route('configurations.edit', [$configuration]) }}">Edit</a></td>
                </tr>
            @endforeach
        </table>

    </div>


@endsection