@extends('layouts.app')

@section('content')

    <div class="container">

        <h1>Equipments</h1>

        @if(Session::has('message'))
            <div class="{{ Session::get('class') }}">
                <p>{{ Session::get('message') }}</p>
            </div>
        @endif


        <a href="{{ route('equipment.add') }}">Add new +</a>

        <table class="table table-bordered">
            <tr>
                <th>Id</th>
                <th>Title</th>
                <th>Name</th>
                <th>Room</th>
                <th>Actions</th>
            </tr>
            @foreach($equipment as $equipment)
                <tr>
                    <td>{{ $equipment->id }}</td>
                    <td>{{ $equipment->ae_title }}</td>
                    <td>{{ $equipment->name }}</td>
                    <td>{{ $equipment->room->name }}</td>
                    <td><a href="{{ route('equipment.show', [$equipment]) }}">Show</a> | <a href="{{ route('equipment.edit', [$equipment]) }}">Edit</a> | <a href="{{ route('equipment.delete', [$equipment]) }}" onclick="return confirm('{{ trans("messages.delete") }}')">Delete</a></td>
                </tr>
            @endforeach
        </table>

    </div>


@endsection