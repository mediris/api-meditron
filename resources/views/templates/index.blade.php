@extends('layouts.app')

@section('content')

    <div class="container">

        <h1>{{ ucfirst(trans('messages.templates')) }}</h1>

        @if(Session::has('message'))
            <div class="{{ Session::get('class') }}">
                <p>{{ Session::get('message') }}</p>
            </div>
        @endif

        <a href="{{ route('templates.add') }}">{{ ucfirst(trans('messages.add')) }} +</a>

        <table class="table table-bordered">
            <tr>
                <th>Id</th>
                <th>Description</th>
                <th>Actions</th>
            </tr>
            @foreach($templates as $template)
                <tr>
                    <td>{{ $template->id }}</td>
                    <td>{{ $template->description }}</td>
                    <td><a href="{{ route('templates.show', [$template]) }}">{{ ucfirst(trans('messages.show')) }}</a> | <a href="{{ route('templates.edit', [$template]) }}">{{ ucfirst(trans('messages.edit')) }}</a> | <a href="{{ route('templates.delete', [$template]) }}" onclick="return confirm('{{ trans("messages.delete") }}')">{{ ucfirst(trans('messages.purge')) }}</a></td>
                </tr>
            @endforeach
        </table>

    </div>


@endsection